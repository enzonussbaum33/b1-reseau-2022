# TP1 - Premier pas réseau

# Sommaire

- [TP1 - Premier pas réseau](#tp1---premier-pas-réseau)
- [Sommaire](#sommaire)
- [Déroulement et rendu du TP](#déroulement-et-rendu-du-tp)
- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [En ligne de commande](#en-ligne-de-commande)
    - [En graphique (GUI : Graphical User Interface)](#en-graphique-gui--graphical-user-interface)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [1. Prérequis](#1-prérequis)
  - [2. Câblage](#2-câblage)
  - [Création du réseau (oupa)](#création-du-réseau-oupa)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)
- [Bilan](#bilan)

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

**🌞 Affichez les infos des cartes réseau de votre PC**

- nom, adresse MAC et adresse IP de l'interface WiFi

```bash
[it4@nowhere]$ ip a
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether ⭐e4:b3:18:48:36:68⭐ brd ff:ff:ff:ff:ff:ff
    inet ⭐192.168.1.15/24⭐ brd 192.168.1.255 scope global dynamic noprefixroute wlan0
       valid_lft 78437sec preferred_lft 78437sec
```

- nom, adresse MAC et adresse IP de l'interface Ethernet

```bash
[it4@nowhere]$ ip a
2: enp0s31f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether ⭐c8:5b:76:1b:3c:81⭐ brd ff:ff:ff:ff:ff:ff

# La carte Ethernet, n'étant branchée, pas utilisée, n'a pas d'adresse IP.
```

**🌞 Affichez votre gateway**

```bash
[it4@nowhere]$ ip r s 
default via ⭐192.168.1.1⭐ dev wlan0 proto dhcp src 192.168.1.15 metric 600 
```

**🌞 Déterminer la MAC de la passerelle**

```bash
[it4@nowhere ~]$ ip n s 
192.168.1.1 dev wlan0 lladdr ⭐78:94:b4:de:fd:c4⭐REACHABLE 
```

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

➜ On perd l'accès Internet si on choisit manuellement une adresse IP que quelqu'un utilise déjà. Hors les adresses IP des clients d'un LAN doivent toutes être différentes.

# II. Exploration locale en duo

## 3. Modification d'adresse IP

🌞 **Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau**

PC1 : 

```bash
$ ip addr add 10.10.10.1/24 dev enp0s31f6
```

PC2 :

```
$ ip addr add 10.10.10.2/24 dev eth0
```

🌞 **Vérifier à l'aide d'une commande que votre IP a bien été changée**

PC1 :

```bash
[it4@nowhere ~]$ ip a
2: enp0s31f6: <LOOPBACK,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether c8:5b:76:1b:3c:81 brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.1/24 brd 10.10.10.255 scope global
```

PC2 :

```bash
[it4@nowhere ~]$ ip a
2: enp0s31f6: <LOOPBACK,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
    link/ether 90:19:12:cc:21:7d brd ff:ff:ff:ff:ff:ff
    inet 10.10.10.2/24 brd 10.10.10.255 scope global
```

🌞 **Vérifier que les deux machines se joignent**

```bash
[it4@nowhere ~]$ ping 10.10.10.2
PING 10.10.10.2 (10.10.10.2) 56(84) bytes of data.
64 bytes from 10.10.10.2: icmp_seq=1 ttl=64 time=4.49 ms
64 bytes from 10.10.10.2: icmp_seq=2 ttl=64 time=4.43 ms
64 bytes from 10.10.10.2: icmp_seq=3 ttl=64 time=4.53 ms

--- 10.10.10.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 4.430/4.483/4.531/0.041 ms
```

🌞 **Déterminer l'adresse MAC de votre correspondant**

```bash
[it4@nowhere]$ ip n s
192.168.1.1 dev wlan0 lladdr 78:94:b4:de:fd:c4 REACHABLE 
10.10.10.2 dev enp0s31f6: lladdr 90:19:12:cc:21:7d REACHABLE
```

## 4. Utilisation d'un des deux comme gateway

🌞**Tester l'accès internet**


```bash
[it4@nowhere]$ ip r s 
default via 10.10.10.2 dev enp0s31f6

[it4@nowhere ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=64 time=4.43 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=64 time=4.65 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=64 time=4.47 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 4.432/4.516/4.647/0.093 ms
```

🌞 **Prouver que la connexion Internet passe bien par l'autre PC**

```bash
[it4@nowhere ~]$ traceroute 1.1.1.1
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  10.10.10.2 (10.10.10.2)  4.439 ms  4.508 ms  4.653 ms
 2  192.168.1.1 (192.168.1.1)  6.144 ms  6.101 ms  6.067 ms
 3  lag-10.nebrd30z.rbci.orange.net (193.253.94.82)  6.034 ms  6.001 ms  5.966 ms
 4  ae93-0.ncbor201.rbci.orange.net (193.253.94.58)  18.045 ms  18.009 ms  17.975 ms
 5  ae42-0.nipoi201.rbci.orange.net (193.252.100.25)  14.865 ms  14.831 ms  14.795 ms
 6  ae40-0.nipoi202.rbci.orange.net (193.252.160.46)  14.759 ms  8.810 ms  8.727 ms
 7  193.252.137.14 (193.252.137.14)  17.001 ms  16.952 ms  16.906 ms
 8  bundle-ether305.partr2.saint-denis.opentransit.net (193.251.133.23)  16.855 ms  17.748 ms  17.669 ms
 9  cloudflare-18.gw.opentransit.net (193.251.150.160)  17.631 ms  17.597 ms  17.560 ms
10  172.71.128.2 (172.71.128.2)  20.743 ms 172.71.120.2 (172.71.120.2)  34.862 ms  34.800 ms
11  one.one.one.one (1.1.1.1)  17.861 ms  16.813 ms  16.749 ms
```

## 5. Petit chat privé

Serveur :

```bash
[it4@nowhere ~]$ nc -l 10.10.10.1 8888
mao
i'm the server
i'm the client
```

Client :

```bash
[it4@nowhere ~]$ nc 10.10.10.1 8888
mao
i'm the server
i'm the client
```


🌞 **Visualiser la connexion en cours**  
🌞 **Pour aller un peu plus loin**

```bash
[it4@nowhere ~]$ sudo ss -alnpt | grep 8888
LISTEN 0      1          10.10.10.1:8888      0.0.0.0:*    users:(("nc",pid=1013906,fd=3))
```

## 6. Firewall

🌞 **Activez et configurez votre firewall**

```bash
# démarrage du firewall
$ sudo systemctl start firewalld

# firewald autorise les pings par défaut

# ajout du port 8888/tcp
$ sudo firewall-cmd --add-port-8888/tcp
```

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

🌞**Exploration du DHCP, depuis votre PC**

```bash
[it4@nowhere ~]$ nmcli con show "Livebox-FDC4" | grep -i dhcp
[...]
DHCP4.OPTION[2]:                        dhcp_lease_time = 86400
DHCP4.OPTION[3]:                        dhcp_server_identifier = 192.168.1.1
[...]
DHCP4.OPTION[6]:                        expiry = 1666590996
[...]
```

## 2. DNS

🌞** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**

```bash
[it4@nowhere ~]$ nmcli con show "Livebox-FDC4" | grep -i dns
[...]
IP4.DNS[1]:                             192.168.1.1
```

🌞 Utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

```bash
[it4@nowhere ~]$ dig google.com
[...]
;; ANSWER SECTION:
google.com.		252	IN	A	142.250.179.110

;; SERVER: 192.168.1.1#53(192.168.1.1) (UDP)

[it4@nowhere ~]$ dig ynov.com
[...]
;; ANSWER SECTION:
ynov.com.		300	IN	A	172.67.74.226
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	104.26.10.233

;; SERVER: 192.168.1.1#53(192.168.1.1) (UDP)
```

Les requêtes sont faites au serveur `192.168.1.1` visible sur la ligne `;; SERVER: 192.168.1.1#53(192.168.1.1) (UDP)`.

```bash
[it4@nowhere ~]$ dig -x 231.34.113.12 # pas de réponse pour celui-ci
[...]
;; AUTHORITY SECTION:
231.in-addr.arpa.	3600	IN	SOA	sns.dns.icann.org. noc.dns.icann.org. 2022091121 7200 3600 604800 3600

;; SERVER: 192.168.1.1#53(192.168.1.1) (UDP)

[it4@nowhere ~]$ dig -x 78.34.2.17
[...]
;; ANSWER SECTION:
17.2.34.78.in-addr.arpa. 3600	IN	PTR	cable-78-34-2-17.nc.de.

;; SERVER: 192.168.1.1#53(192.168.1.1) (UDP)
```

# IV. Wireshark

## 1. Intro Wireshark

🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

- [un `ping` entre vous et votre passerelle](./pcap/ping_gw.pcapng)
- [un `netcat` entre vous et votre mate, branché en RJ45](./pcap/netcat.pcapng)
- [une requête DNS](./pcap/dns.pcapng)

