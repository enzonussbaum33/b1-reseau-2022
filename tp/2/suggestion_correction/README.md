# TP2 : Ethernet, IP, et ARP

# Sommaire

- [TP2 : Ethernet, IP, et ARP](#tp2--ethernet-ip-et-arp)
- [Sommaire](#sommaire)
- [I. Setup IP](#i-setup-ip)
- [II. ARP my bro](#ii-arp-my-bro)
- [III. DHCP you too my brooo](#iii-dhcp-you-too-my-brooo)

# I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

Adresse de réseau choisie `10.3.1.0/22`, son broadcast : `10.3.3.255`.

Le PC sera en `10.3.1.1/22`, la VM en `10.3.1.11/22`.

Le PC se configure via VBox. Dans la suite du TP, le PC sera : `[it4@nowhere ~]$`.

La VM se configure depuis la VM elle même. Dans la suite du TP, la VM sera : `[it4@meow ~]$`.

```bash
[it4@meow ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8
NAME=meow

ONBOOT=yes
BOOTPROTO=static

IPADDR=10.3.1.11
NETMASK=255.255.252.0

[it4@meow ~]$ sudo systemctl restart NetworkManager

[it4@meow ~]$ sudo nmcli con up meow
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)

[it4@meow ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7f:b8:b8 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/22 brd 10.3.3.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7f:b8b8/64 scope link
       valid_lft forever preferred_lft forever
```

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

```bash
[it4@meow ~]$ ping 10.3.1.1
PING 10.3.1.1 (10.3.1.1) 56(84) bytes of data.
64 bytes from 10.3.1.1: icmp_seq=1 ttl=64 time=0.593 ms
64 bytes from 10.3.1.1: icmp_seq=2 ttl=64 time=0.543 ms
^C
--- 10.3.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 0.543/0.568/0.593/0.025 ms
```         

- un ping est un message ICMP :
  - la machine qui envoie le `ping` est un ICMP echo request (type 8)
  - la machine qui répond renvoie un ICMP echo reply (type 0)

🦈 **[PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP](./pcap/ping.pcapng)**

# II. ARP my bro

🌞 **Check the ARP table**

```bash
[it4@nowhere ~]$ ip n s 
10.3.1.11 dev vboxnet0 lladdr 08:00:27:7f:b8:b8 STALE # binome (vm)
192.168.1.1 dev wlan0 lladdr 78:94:b4:de:fd:c4 STALE  # passerelle
```

🌞 **Manipuler la table ARP**

p- utilisez une commande pour vider votre table ARP
- prouvez que ça fonctionne en l'affichant et en constatant les changements
- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP

```bash
# vide puis affiche la table ARP
[it4@meow ~]$ sudo ip neigh flush all && ip neigh show
# aucun résultat

[it4@meow ~]$ ping 10.3.1.1
PING 10.3.1.1 (10.3.1.1) 56(84) bytes of data.
64 bytes from 10.3.1.1: icmp_seq=1 ttl=64 time=0.319 ms
64 bytes from 10.3.1.1: icmp_seq=2 ttl=64 time=0.555 ms
^C
--- 10.3.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.319/0.437/0.555/0.118 ms

[it4@meow ~]$ ip neigh show
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 REACHABLE
```

🌞 **Wireshark it**

| Trame | Source | Destination | Signification |
| --- | --- | --- | --- |
| ARP request | `08:00:27:7f:b8:b8` | `ff:ff:ff:ff:ff:ff` | Le PC qui porte la MAC `08:00:27:7f:b8:b8` cherche à qui appartient l'IP 10.3.1.1 |
| ARP reply | `0a:00:27:00:00:00` | `08:00:27:7f:b8:b8` | Le PC `0a:00:27:00:00:00` informe `08:00:27:7f:b8:b8` que  c'est lui 10.3.1.1 | 


🦈 **[PCAP qui contient les trames ARP](./pcap/arp.pcapng)**

# III. DHCP you too my brooo

🌞 **Wireshark it**

| Trame | Src | Dst |
| --- | --- | --- |
| Discover | `e4:b3:18:48:36:68` | `ff:ff:ff:ff:ff:ff` |
| Offer | `78:94:b4:de:fd:c4` | `e4:b3:18:48:36:68` |
| Request | `e4:b3:18:48:36:68` | `78:94:b4:de:fd:c4` |
| Acknowledge | `78:94:b4:de:fd:c4` | `e4:b3:18:48:36:68` |

Les infos **1**, **2** et **3** sont envoyées dans la trame DHCP Offer :

- `Your (client) IP address: 192.168.1.15`
- `Option: (6) Domain Name Server` : `Domain Name Server: 192.168.1.1`
- `Option: (3) Router` : `Router: 192.168.1.1`

🦈 **[PCAP qui contient l'échange DORA](./pcap/dora.pcapng)**
